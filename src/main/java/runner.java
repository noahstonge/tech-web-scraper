import java.util.ArrayList;

public class runner {


    public static int breakOutInt = -1;
    static BoardLayersListeners gui;
    static boolean openLinks = true;
    static runner run;
    static stockSearchModel model;
    static ArrayList<String> links = new ArrayList<>();

    /**
     *
     * @param args command line args passed in
     *
     *             main function of the controller that stars the whole program
     */
    public static void main(String[] args){
        startProgram();
    }

    /**
     * calls all the functions to begin web scraping and set up the gui and all relevant information
     */
    private static void startProgram(){
        run = new runner();
        model = new stockSearchModel(0, null, run, null);
        gui = new BoardLayersListeners(run);
        model.setGuiVisible(gui);
        waitForInput();
        if(breakOutInt < 1) return;
        ArrayList<String> elements;
        links = getProperList(model, gui);
        if(!validateLinks(links, model)){
            gui.dispose();
            return;
        }
        if(links.size()<1) return;
        elements = model.buildElementList(links);
        Thread[] threads = initThread(links, elements);
        startThread(threads);
    }

    /**
     *
     * @param links links we want to scrape
     * @param model model to use to try and connect to all the links
     * @return returns false if any link is invalid as in null
     *
     *                      we take each link and connect to it to make sure it is valid
     *                      if not valid return false otherwise keep going through whole list
     *                      and return true if all links are valid
     */
    private static boolean validateLinks(ArrayList<String> links, stockSearchModel model){
        if(links == null || links.isEmpty()) return false;
        for (String link : links) {
            if (link == null) return false;

            if(!model.testConnect(link)) return false;

        }
        return true;
    }

    /**
     *
     * @param threads threads that are the links we are to scrape and the logic to do so
     *
     *                this will start each thread causing it to start scraping
     */
    private static void startThread(Thread[] threads){
        if(threads.length < 1){
            return;
        }
        for (Thread thread : threads) {
            thread.start();
        }
    }

    /**
     *
     * @param model our model object
     * @param gui the single gui
     * @return returns the proper list of links when the user wants a custom link list
     */
    private static ArrayList<String> getProperList(stockSearchModel model, BoardLayersListeners gui){
        if(gui == null || model == null) return null;
        ArrayList<String> link;
        if(breakOutInt == Integer.MAX_VALUE){
            link = model.getDefaultList();
            gui.labelSetup(link.size());
            gui.removeButtons();
            gui.setVisibleAll();
        }
        else{
            link = gui.getLinks(breakOutInt);
        }

        return link;
    }

    /**
     *
     * @param links links we are to scrape
     * @param elements elements associated with the links we are to scrape that are used for telling if it is in stock
     * @return the array of threads that are used to actually scrape each link individually
     *
     *              creates a thread for every link and in each thread is a model object that we create in the loop
     */
    public static Thread[] initThread(ArrayList<String> links, ArrayList<String> elements){
        if(links == null || links.isEmpty() || elements == null || elements.isEmpty()) return null;

        Thread[] threads = new Thread[links.size()];
        for(int i = 0; i< links.size(); i++){
            threads[i] = new Thread(new stockSearchModel(i, links.get(i), run, elements.get(i)));
        }
        return threads;
    }

    /**
     *
     * @param value integer value that is passed from the gui, so we know the input that the user put in
     *
     * This function takes in an int that is the number of links to scrape this also is how we break out of the
     * loop that lets us wait for user input
     */
    public void setBreakOutInt(int value){
        breakOutInt = value;
    }

    /**
     *
     * @param index index of the link we want to open to a website
     */
    public void openWebsite(int index){
        if(index < 0 || index >= links.size()){}
        model.openLink(links.get(index));
    }

    /**
     * waits until breakoutint gets set to a value and will then break out of this function
     */
    public static void waitForInput(){
        try {
            while (breakOutInt < 0) {
                try {
                    //noinspection BusyWait
                    Thread.sleep(200);
                } catch (InterruptedException ignored) {
                }
            }
        } catch (Exception ignored) {}
    }

    /**
     *
     * @param buttonIndex index to a specific button on the gui
     * @param toSet string we want to set the button to say
     *
     *              sets a specific button to a given string for what it displays, this will tell us what
     *              object was scraped that correlates to this button
     */
    public synchronized void updateButton(int buttonIndex, String toSet){
        if(buttonIndex < 0 || buttonIndex >= links.size()) return;
        gui.updateGui(buttonIndex, toSet);
    }

    /**
     *
     * @return returns if we are supposed to be opening links when scraping or not automatically
     */
    public boolean toOpen() {
        return openLinks;
    }

    /**
     * sets if we are supposed to open links automatically to false
     */
    public void dontOpen() {
        openLinks = false;
    }
    /**
     * sets if we are supposed to open links automatically to true
     */
    public void doOpen() {
        openLinks = true;
    }
}
