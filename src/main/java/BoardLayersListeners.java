/*
Gui built for an in stock checker of items on amazon newegg and bestbuy created by
Stongen 12/15/2020

*/

import org.jetbrains.annotations.NotNull;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class BoardLayersListeners extends JFrame {
    //array of labels array and instance of in-stock class to
//allow for conversation between two classes of mvc
    public static JButton[] labels = null;
    public static runner run;

    // JLabels
    JLabel programSetup;

    //JButtons
    JButton defaultList;
    JButton customList;
    JButton noAutoOpen;
    JButton autoOpen;

    // JLayered Pane
    JLayeredPane bPane;

    // Constructor

    public BoardLayersListeners(runner runObj) {

        // Set the title of the JFrame
        super("StockChecker");
        // Set the exit option for the JFrame
        run = runObj;
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        // Create the JLayeredPane to hold the display, cards, dice and buttons
        bPane = getLayeredPane();
        setSize(1000,420);

        //just a label that is for showing where program setup is
        programSetup = new JLabel("Program Setup");
        programSetup.setBounds(40,30,140,20);
        bPane.add(programSetup,2);

        //create and add default list button to the gui
        defaultList = new JButton("Default List");
        defaultList.setBackground(Color.white);
        defaultList.setBounds(10, 60,140, 20);
        defaultList.addMouseListener(new boardMouseListener());


        //create and add custom list button to the gui
        customList = new JButton("Custom List");
        customList.setBackground(Color.white);
        customList.setBounds(10,90,140, 20);
        customList.addMouseListener(new boardMouseListener());

        //create the don't auto open link button to the gui
        noAutoOpen = new JButton("Don't Auto Open Available Products");
        noAutoOpen.setBackground(Color.white);
        noAutoOpen.setBounds(300,0,240, 20);
        noAutoOpen.addMouseListener(new boardMouseListener());

        //create the auto open link button to the gui
        autoOpen = new JButton("Auto Open Available Products");
        autoOpen.setBackground(Color.white);
        autoOpen.setBounds(10,0,240, 20);
        autoOpen.addMouseListener(new boardMouseListener());


        // Place the action buttons in the top layer
        bPane.add(defaultList,2);
        bPane.add(customList,2);
        bPane.add(noAutoOpen,2);
        bPane.add(autoOpen, 2);
    }

    // This class implements Mouse Events

    class boardMouseListener implements MouseListener{

        // Code for the different button clicks
        public void mouseClicked(@NotNull MouseEvent e) {

            if (e.getSource()== defaultList){//default list
                run.setBreakOutInt(Integer.MAX_VALUE);
            }
            else if (e.getSource()== customList){//custom list
                if(numLinks()) {
                    removeButtons();
                    setVisibleAll();
                }

            }
            else if (e.getSource()== noAutoOpen){
                run.dontOpen();
            }
            else if(e.getSource() == autoOpen){
                run.doOpen();
            }
            else{ //check which button was clicked to set it to not auto open and update the text
                openWebsitePressed(e);
            }
        }
        public void mousePressed(MouseEvent e) {
        }
        public void mouseReleased(MouseEvent e) {
        }
        public void mouseEntered(MouseEvent e) {
        }
        public void mouseExited(MouseEvent e) {
        }
    }

    private void openWebsitePressed(MouseEvent e){
        for(int i=0;i<labels.length;i++){
            if(e.getSource() == labels[i] && (!labels[i].getText().equals(""))){
                run.openWebsite(i);
            }
        }
    }

    //outputs the ask of how many links the person wants to input for custom list
    //and will send the value back to the model for use
    public boolean numLinks(){
        int value;
        try{
          value = Integer.parseInt(JOptionPane.showInputDialog(bPane, "How many links do you want to check"));
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(bPane,"Invalid input try again");
            return false;
        }
        if(value < 0){
            return false;
        }
        labelSetup(value);
        run.setBreakOutInt(value);

        return true;
    }
    //this takes in the link from the user and if it is from amazon newegg or bestbuy
    //then will send it back to the model to be used later
    public ArrayList<String> getLinks(int numberOfLinks){
        ArrayList<String> links = new ArrayList<>();
        for(int i = 0; i < numberOfLinks; i++) {
            String value = JOptionPane.showInputDialog(bPane, "Please copy and paste the item url from either Amazon, Newegg, or Best Buy");
            links.add(value);
        }
        return links;
    }
    //makes the buttons invisible
    public void removeButtons(){
        programSetup.setVisible(false);
        defaultList.setVisible(false);
        customList.setVisible(false);
    }
    //creates the labels for all the amount of links that the user wants and will
    //set up where they are displayed on the gui, so it doesn't overlap
    public void labelSetup(int labelCount) {
        labels = new JButton[labelCount];
        for(int i=0;i<labelCount;i++){
            labels[i] = new JButton();
            labels[i].addMouseListener(new boardMouseListener());
            JButton temp = labels[i]; // temp should reference
            temp.setText("");
            if(labelCount == 1 || i < labelCount/2){
                temp.setBounds(10,30 * (i+1)+30,450, 20);
            }
            else{
                temp.setBounds(500,30 * ((i+1)-labelCount/2)+30,450, 20);
            }
        }

    }
    //sets the labels to be visible for every label
    //we created in the label setup above
    public void setVisibleAll(){
        for (JButton temp : labels) {
            temp.setVisible(true);
            bPane.add(temp, 3);
        }
    }
    //updates the gui for a given index in the labels array
    //and will update what it says
    public void updateGui(int index, String nameToSet){
        JButton temp = labels[index];
        temp.setText(nameToSet);
    }
}