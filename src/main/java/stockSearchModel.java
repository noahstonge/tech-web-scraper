import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner; // Import the Scanner class to read text files

/**
 * denyList is the deny list of strings we want to keep out of the button we are displaying for what product we scraped
 */
public class stockSearchModel implements Runnable {

    int threadNumber;
    String link;
    String element;
    static runner runObj;
    static ArrayList<String> denyList;

    /**
     *
     * @param threadNumber int passed when creating threads which will be used later
     * @param link the website link for this object we want to scrape
     * @param run an instance of the runner object to allow the object to connect to the controller
     * @param element the website element used for finding out if the item is purchasable
     */
    public stockSearchModel(int threadNumber, String link, runner run, String element){
        this.threadNumber = threadNumber;
        this.link = link;
        this.element = element;
        runObj = run;
        denyList = createDenyList();
    }

    /**
     *
     * @param gui the gui object which allows us to set it visible to the user
     */
    public void setGuiVisible(BoardLayersListeners gui){
        if(gui == null) return;
        gui.setVisible(true);
    }

    /**
     *
     * @param links list of links the user inputs from the popup text boxes
     * @return returns an array list containing the elements used to scrape the website to find data we want
     */
    public ArrayList<String> buildElementList(ArrayList<String> links){
        if(links == null || links.size() < 1) return new ArrayList<>();
        ArrayList<String> elements = new ArrayList<>();
        for (String s : links) {
            if (s.toLowerCase().contains("amazon")) { //amazon link
                elements.add("a-button-text");
            } else if (s.toLowerCase().contains("newegg")) { //newegg link
                elements.add("nav-col");
            } else if (s.toLowerCase().contains("bestbuy")) { //bestbuy link
                elements.add("fulfillment-add-to-cart-button");
            } else {
                elements.add(" ");
            }
        }

        return elements;
    }

    /**
     *
     * @return returns a random user agent which is used to get around being timed out of a website
     */
    private String getRandomUserAgent(){
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder userAgent = new StringBuilder();
        userAgent.append("Mozilla");
        for(int j=0;j<(int)(Math.random() * 11) + 1;j++){
            userAgent.append(alphabet[(int)(Math.random() * 26)]);
        }

        return userAgent.toString();
    }

    /**
     *
     * @param userAgent string which is the user agent the website sees when we connect to it using jsoup
     * @return return the document object that is the result of the scraping
     */
    public Document openAndScrapeLink(String userAgent) {
        Document websiteDoc = null;
        try {
            websiteDoc = Jsoup.connect(this.link).userAgent(userAgent).data("name", "jsoup").get();
        } catch (IOException ignored) {}
        return websiteDoc;
    }

    /**
     *
     * @param website document object that is returned when scraping using JSoup
     * @return returns the string which is the title of the website document object
     */
    public String extractStringFromDocument(Document website){
        if(website == null) return " ";
        String toRet = "";
        toRet = toRet + website.title();
        return toRet;
    }

    /**
     *
     * @return returns the deny list which contains strings to be used for removal from the document after we scrape out the string we want
     */
    public ArrayList<String> createDenyList(){
        ArrayList<String> toRet = new ArrayList<>();

        //when working with intelliJ make sure the file that contains text files is marked as resource root,
        //so we can reference it like is done
        try{
            File myObj = new File("./files/denyList.txt");
            Scanner myReader = new Scanner(myObj);
            while(myReader.hasNextLine()){
                toRet.add(myReader.nextLine());
            }
        }
        catch(FileNotFoundException ignored){}

        return toRet;
    }

    //opens the website for this object
    public void openLink(){
        try{
            URI uri = new URI(link);
            try {
                java.awt.Desktop.getDesktop().browse(uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch(URISyntaxException ignored){}
    }
    //overloading of the above function, takes in a link and opens the website in browser
    public void openLink(String link){
        if(link == null) return;
        try{
            URI uri = new URI(link);
            try {
                java.awt.Desktop.getDesktop().browse(uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch(URISyntaxException ignored){}
    }

    /**
     *
     * @param link link we are to test if it is valid
     * @return true if we can connect, false if we can't connect
     *
     *              pretty much try and connect to each link to be sure
     *              all links are valid before we proceed with scraping for
     *              checking if there is stock
     */
    public boolean testConnect(String link){
        try {
            Jsoup.connect(link).userAgent("tester").data("name", "jsoup");
        } catch (Exception e) { //use generic exception as there are multiple we want to catch including invalid links being passed to us
            return false;
        }
        return true;
    }

    /**
     *
     * @param toParse takes in the string we scrape from the website which contains the name and info on the item we are searching for
     * @return returns a parsed version of that string removing items from the deny list
     */
    public String parseString(String toParse){
        if(toParse == null || toParse.length() == 0) return "";
        String[] split = toParse.split(" ");
        StringBuilder parsed = new StringBuilder();

        for (String s : split) {
            boolean toAdd = true;
            for (String value : denyList) {
                if (s.toLowerCase().contains(value)) {
                    toAdd = false;
                    break;
                }
            }
            if (toAdd) {
                parsed.append(s).append(" ");
            }

        }


        return parsed.toString();
    }

    /**
     *
     * @return returns the default list of links to scrape from the text file default.txt
     */
    public ArrayList<String> getDefaultList(){
        ArrayList<String> defaultLinks = new ArrayList<>();
        try{
            File myObj = new File("./files/default.txt");
            Scanner myReader = new Scanner(myObj);
            while(myReader.hasNextLine()){
                defaultLinks.add(myReader.nextLine());
            }
        }
        catch(FileNotFoundException ignored){}

        return defaultLinks;
    }

    /**
     *
     * @param doc this is the document object we get from scraping using jsoup
     * @return returns true if the item can be purchased or false if we find that it is unable to be purchased based on string matching
     */
    private boolean canPurchase(Document doc){
        if(doc == null) return false;
        Elements webElement = doc.getElementsByClass(element);
        if(webElement.isEmpty()) return false;
        String potentialPrice = webElement.get(0).text();
        if(potentialPrice.contains("sold")) return false;
        if(potentialPrice.contains("not")) return false;
        if(potentialPrice.contains("Add to List")) return false;
        return !potentialPrice.contains("See All Buying Options"); //as this is the last string to check return result of contains here
    }

    //should be passed link to scrape, element to use
    @Override
    public void run(){
        //noinspection InfiniteLoopStatement
        while(true) {
            //call method to scrape from website and return document object model uses a random user agent to get around failures
            Document websiteDoc = openAndScrapeLink(getRandomUserAgent());

            //call method to extract string we want from document object model
            String toParse = extractStringFromDocument(websiteDoc);

            //call method to parse string and remove data we don't want in it
            String parsed = parseString(toParse);

            //call method to update gui using thread number as the index for gui button to update (be sure to synchronize the class updating gui)
            runObj.updateButton(this.threadNumber, parsed);

            //call method to check if this link is available for purchase
            boolean available = canPurchase(websiteDoc);

            //call method to open up website link if that specific button is not set to false and the link is available for purchase
            if (available && runObj.toOpen()) {
                openLink();
            }
            else if(!available){runObj.updateButton(this.threadNumber, "Out Of Stock: " + parsed);}


            //want to wait before looking for update
            try {
                //noinspection BusyWait
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
