import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class unitTests {

    /**
     *
     * @param args command line arguments
     *
     *             main function that when called will initiate unit tests
     */
    public static void main(String[] args){
        stockSearchModel testModel = new stockSearchModel(0,"https://www.google.com",null,null);
        testModelLogic(testModel);
        testRunnerLogic();
    }

    /**
     *
     * @param testModel model object that is used and passed to the unit tests for testing the functions
     */
    public static void testModelLogic(stockSearchModel testModel){
        testBuildElement(testModel);
        testOpenAndScrape(testModel);
        testExtractStringFromDocument(testModel);
        testCreateDenyList(testModel);
        testParseString(testModel);
        testGetDefaultList(testModel);
    }

    /**
     * calls all runner unit test functions
     */
    public static void testRunnerLogic(){
        testInitThread();
    }

    /**
     *
     *
     *
     *            this method test the initThread function in the runner class
     */
    private static void testInitThread(){
        assertNull(runner.initThread(null, null));
        assertNull(runner.initThread(new ArrayList<>(), null));
        assertNull(runner.initThread(null, new ArrayList<>()));
        assertNull(runner.initThread(new ArrayList<>(), new ArrayList<>()));
        ArrayList<String> testList = new ArrayList<>();
        testList.add("aaa");
        assertEquals(1, runner.initThread(testList,testList).length);
    }
    /**
     *
     * @param testModel object of model to be used for running the unit tests
     *
     *                  this class will contain unit tests for the build elements function
     *                  in the model class
     */
    private static void testBuildElement(stockSearchModel testModel){
        assertEquals(new ArrayList<>(), testModel.buildElementList(new ArrayList<>()));
        assertEquals(new ArrayList<>(), testModel.buildElementList(null));
        ArrayList<String> testCompanies = new ArrayList<>();
        ArrayList<String> testElements = new ArrayList<>();
        testCompanies.add("amazon");
        testCompanies.add("newegg");
        testCompanies.add("bestbuy");
        testElements.add("a-button-text");
        testElements.add("nav-col");
        testElements.add("fulfillment-add-to-cart-button");
        assertEquals(testElements, testModel.buildElementList(testCompanies));
        testCompanies = new ArrayList<>();
        testCompanies.add(" ");
        assertEquals(testCompanies.get(0), testModel.buildElementList(testCompanies).get(0));
    }

    /**
     *
     * @param testModel object of model to be used for running the unit tests
     *
     *                  tests the openAndScrape function in model
     */
    private static void testOpenAndScrape(stockSearchModel testModel){
        assertNotNull(testModel.openAndScrapeLink("testAgent"));
    }

    /**
     *
     * @param testModel object of model to be used for running the unit tests
     *
     *                  tests extract string function in the model
     */
    private static void testExtractStringFromDocument(stockSearchModel testModel){
        assertEquals(" ", testModel.extractStringFromDocument(null));
    }

    /**
     *
     * @param testModel object of model to be used for running the unit tests
     *
     *                  tests the create deny list method in the model
     */
    private static void testCreateDenyList(stockSearchModel testModel){
        assertTrue(testModel.createDenyList().size() > 0);
    }

    private static void testGetDefaultList(stockSearchModel testModel){
        assertTrue(testModel.getDefaultList().size() > 0);
    }

    /**
     *
     * @param testModel object of model to be used for running the unit tests
     *
     *                  tests the parse string function in the model
     */
    private static void testParseString(stockSearchModel testModel){
        assertEquals("", testModel.parseString(null));
        assertEquals("", testModel.parseString(""));
        assertEquals("", testModel.parseString("gaming"));
        assertEquals("", testModel.parseString("""
                gaming
                 amazon
                 bestbuy
                 newegg
                 lhr
                 rev
                 gv"""));
        assertEquals("my special test ", testModel.parseString("my gaming special gaming test"));
        assertEquals("", testModel.parseString("mygamingspecialgamingtest"));
    }

}
